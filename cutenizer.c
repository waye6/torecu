#include "cutenizer.h"

/* cute or no / callable (ie) */
Stoken *cuono(Stoken *token)
{
  static Stoken T;
  if(tt.NUMBER & ttnScallable)
    return ttc.cutenizer(token);
  else
    return token;
}
