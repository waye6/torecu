#include "associate.h"

associate_t associate[associate_last_index + 1];

CU(associate)
{
  static Stoken RETU;
  TT(&RETU).NUMBER = ttnSsymbol;
  TT(&RETU).symbol.s = TTcuono(ttcv(0)).symbol.s;
  associate['z' - TT(&RETU).symbol.s].value =
    *cuono(syono(ttcv(1)));
  return &RETU;
}
RE(associate)
{
  reaffix2callable;
  if(0 == ttnchk(ttcv(0),ttnSsymbol,ttnSproperty_type))
    errxit("expected ttnSsymbol,ttnSproperty_type");
  recallable(ttnSnil,CUassociate);
}
TO(associate)
{
  totoken(ttnSaffix);
  toaffix(INFIX,1,0,REassociate);
}

Stoken *syono(Stoken *token)
{
  if(tt.NUMBER & ttnSsymbol)
    return &(associate['z' - tt.symbol.s].value);
  else
    return token;
}
