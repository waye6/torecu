


#include "print.h"

CU(print)
{
  if(TTcuono(ttcv(0)).NUMBER & ttnSnumber)
    printf("%d\n",TTcuono(ttcv(0)).number.n);
  if(TTcuono(ttcv(0)).NUMBER & ttnSsymbol)
    printf("%d\n",TTcuono(syono(ttcv(0))).number.n);
  
  if(TTcuono(ttcv(0)).NUMBER & ttnSstring)
    {
      char *p = TTcuono(syono(ttcv(0))).string.s;
      char c;
      for(;*p;++p)
	{
	  while(*p == '\\')
	    {
	      ++p;
	      switch(*p){
	      case '\\':
		c = '\\';
		goto gstat;
	      case 'n':
		c = '\n';
		goto gstat;
	      default:
		{
		gstat:
		  strcpy(p-1,p);
		  *(p-1) = c;
		}
		break;
	      }
	    }
	}
      printf("%s",TTcuono(syono(ttcv(0))).string.s);
    }
}
RE(print)
{
  reaffix2callable;
  if(!(TTcuono(ttcv(0)).NUMBER & ttnSproperty_type))
    errxit("non-type passed to PRINT");
  recallable(ttnSnil,CUprint);
}
TO(print)
{
  puts("found PRINT");
  totoken(ttnSaffix);
  toaffix(PREFIX,1,0,REprint);
}
