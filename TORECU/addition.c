#include "addition.h"

CU(addition_infix)
{
  static Stoken RETU;
  TT(&RETU).NUMBER = ttnSnumber | ttnSproperty_type;
  TT(&RETU).number.n = TTcuono(syono(ttcv(0))).number.n
    + TTcuono(syono(ttcv(1))).number.n;
  return &RETU;
}
CU(addition_prefix)
{
  static Stoken RETU;
  TT(&RETU).NUMBER = ttnSnumber | ttnSproperty_type;
  TT(&RETU).number.n = TTcuono(syono(ttcv(0))).number.n;
  return &RETU;
}
RE(addition)
{
  reicanp(ttnSnumber|ttnSsymbol);
  reaffix2callable;
  if(ttc.argc == 2){
    recallable(ttnSsymbol|ttnSnumber|ttnSproperty_type,CUaddition_infix);
  }
  else{
    recallable(ttnSsymbol|ttnSnumber|ttnSproperty_type,CUaddition_prefix);
  }
}
TO(addition)
{
  totoken(ttnSaffix);
  toaffix(INFIX,1,1,REaddition);
}
