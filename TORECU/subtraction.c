#include "subtraction.h"

CU(subtraction_infix)
{
  static Stoken RETU;
  TT(&RETU).NUMBER = ttnSnumber | ttnSproperty_type;
  TT(&RETU).number.n = TTcuono(syono(ttcv(0))).number.n
    - TTcuono(syono(ttcv(1))).number.n;
  return &RETU;
}
CU(subtraction_prefix)
{
  static Stoken RETU;
  TT(&RETU).NUMBER = ttnSnumber | ttnSproperty_type;
  TT(&RETU).number.n = -TTcuono(syono(ttcv(0))).number.n;
  return &RETU;
}
RE(subtraction)
{
  reicanp(ttnSnumber|ttnSsymbol);
  reaffix2callable;
  if(ttc.argc == 2){
    recallable(ttnSsymbol|ttnSnumber|ttnSproperty_type,CUsubtraction_infix);
  }
  else{
    recallable(ttnSsymbol|ttnSnumber|ttnSproperty_type,CUsubtraction_prefix);
  }
}
TO(subtraction)
{
  totoken(ttnSaffix);
  toaffix(INFIX,1,1,REsubtraction);
}
