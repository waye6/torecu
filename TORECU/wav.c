#include "wav.h"

CU(wav)
{
  static Stoken RETU;
  TT(&RETU).NUMBER = ttnSsymbol|ttnSproperty_type;
  return &RETU;
}
RE(wav)
{
  reaffix2callable;
  recallable(ttnSsymbol|ttnSproperty_type,CUwav);
}
TO(wav)
{
  totoken(ttnSaffix);
  toaffix(PREFIX,1,0,REwav);
}
