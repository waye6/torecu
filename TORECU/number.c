#include "number.h"

TO(number)
{
  totoken(ttnSnumber | ttnSproperty_type);
  tt.number.n = stoi(to->p);
}
