#include "newline.h"
#include "toxy.h"

TO(newline)
{
  totoken(ttnSend_of_line);
  tox = toxy_start_x; toy += 1;
}
