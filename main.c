#include "miscellaneous.h" //errxit
#include "tokenizer.h"
#include "retokenizer.h"
#include "cutenizer.h"
#include <stdlib.h> //malloc
#include <stdio.h>  //printf
#include <string.h> //strcpy

#include "wave.h"

wave_mixer_t mixer={
  .wave={.spec={.rate=44100,
		.depth=16,
		.channels=1}},
  .spec={.rate=44100,
	 .depth=16,
	 .channels=1},
  .allocated = 0,
  .offset = 0
};

Stok_dclr tok_dclr[]; int len_tok_dclr;

/* add/remove TORECU priorto tok_dclr[] */
#include "TORECU/associate.h"
#include "TORECU/wav.h"
#include "TORECU/print.h"
#include "TORECU/blank.h"
#include "TORECU/comment.h"
#include "TORECU/newline.h"
#include "TORECU/parentheses.h"
#include "TORECU/string.h"
#include "TORECU/symbol.h"
#include "TORECU/number.h"
#include "TORECU/addition.h"
#include "TORECU/subtraction.h"
#include "TORECU/hyphenline.h"

/* declaration of TO */
Stok_dclr tok_dclr[]={
  {.regex="\\+",
   .level=0,
   .length=1,
   .tokenizer=TOaddition},
  {.regex="_",
   .level=0,
   .length=1,
   .tokenizer=TOsubtraction},
  {.regex="-+",//3 or more of - is hyphenline... - is minus -- is decrementor
   .level=0,
   .length=0,
   .tokenizer=TOhyphenline},
  {.regex="WAV",
   .level=0,
   .length=0,
   .tokenizer=TOwav},
  {.regex="=",
   .level=0,
   .length=0,
   .tokenizer=TOassociate},
  {.regex="print",
   .level=0,
   .length=0,
   .tokenizer=TOprint},
  {.regex=" ",
   .level=0,
   .length=1,
   .tokenizer=TOblank},
  {.regex="#",
   .level=0,
   .length=1,
   .tokenizer=TOcomment},
  {.regex="\n+",
   .level=0,
   .length=1,//length supposed to be 1??? idk
   .tokenizer=TOnewline},
  {.regex="(a-z)|(A-Z)", /* "a-z+" */
   .level=1,
   .length=1,    /* LONGEST */
   .tokenizer=TOsymbol},
  {.regex="0-9+",
   .level=1,
   .length=LONGEST,
   .tokenizer=TOnumber},
  {.regex="\"",
   .level=0,
   .length=1,
   .tokenizer=TOstring},
  {.regex="\\)",
   .level=0,
   .length=1,
   .tokenizer=TOparenR},
  {.regex="\\(",
   .level=0,
   .length=1,
   .tokenizer=TOparenL},
}; int len_tok_dclr = sizeof(tok_dclr)/sizeof(Stok_dclr);

#define L l->type.affix.type
#define R r->type.affix.type
LorR // RETURN of 1 is R-side was retokenized in LorR
     // RETURN of 0 is L-side can retokenize after return
     // solve of R-side is R->type.affix.retokenizer and return of 1
     // solve of L-side is return 0
{
  // case for - or + as PREFIX not INFIX
  if(L == PREFIX && (l->type.affix.retokenizer == REaddition
  		     || l->type.affix.retokenizer == REsubtraction))
    {
      puts("LorR L == PREFIX and REaddition REsubtraction");
      return 0;
    }
  if(L == PREFIX && R == INFIX){
    r->type.affix.retokenizer(r);
    return 1;
  }
  // case for precedence for INFIX
  if(L == INFIX && R == INFIX){
    if(l->type.affix.precedence < r->type.affix.precedence){
      r->type.affix.retokenizer(r);
      return 1;
    }
  }
  return 0;
}
#undef R
#undef L

int main()
{
  // text -- remember to terminate with \n or segmentation-fault
  char *p,*q;
  { FILE *stream = fopen("test.torecu","r");
    fseek(stream,0,SEEK_END);
    long length = ftell(stream);
    p = calloc(1,/* \n*/1 + length + /* \n*/1 + /* \0*/1);
    *p = '\n'; /* ( ? \n ? */
    fseek(stream,0,SEEK_SET);
    fread(p+1,1,length,stream);
    p[1 + length] = '\n'; /* ) ? \n ? */
    p[2 + length] = '\0';
    q = p + 2 + length;
    fclose(stream); }
  
  puts("~~~TO"); // tokenize
  Stoken *token;
  token = tokenize(p,q,tok_dclr,len_tok_dclr);
  
  puts("~~~RE"); // retokenize
  token = randlrun(token);
  { //retokenize remaining affixes (important!)
    Stoken *tmp = token;
    while(token){
      if(tt.NUMBER
	 & ttnSaffix)
	tta.retokenizer(token);
      token = token -> nxt;
    }
    token = tmp;
  }

  puts("~~~CU"); // execute
  while(token){
    if(tt.NUMBER
       & ttnScallable)
      {
	ttc.cutenizer(token);
      }
    token = token->nxt;
  }
  
  return 0;
}
