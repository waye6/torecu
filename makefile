CC=gcc

#all: vilde
#	make -C TORECU/ -f makefile

static:
	make -C TORECU/ -f makefile
	make vilde

vilde: main.o tokenizer.o retokenizer.o cutenizer.o miscellaneous.o token.o wave.o TORECU/*.o
	$(CC) -lm -o $@ $^ &> cc.txt
#	$(CC) -lm -o $@ $^ TORECU/*.o &> cc.txt

%.o: %.c
	$(CC) -c $<

.PHONY: clean

clean:
	rm -f *.o vilde
	make -C TORECU/ clean
