#ifndef CUTENIZER_H
#define CUTENIZER_H
#include "token.h"

/* CU interface 
   a CU is C-function named cutenizer */
#define CU(foo) Stoken *CU##foo (Stoken *token)
#define ttnof(TOKEN)\
  (TT(TOKEN).NUMBER & ttnScallable ? TT(TOKEN).callable.retu : TT(TOKEN).NUMBER)

/* C-function declaration */
Stoken *cuono(Stoken *token);

#endif
